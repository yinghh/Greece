package com.yhh.data.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
@Getter
@Setter
public class Book implements Serializable {
    private static final long serialVersionUID = 7251922943477570209L;
    @Id
    private Integer id;
    private String name;
    @Column(name = "create_time", nullable = false, length = 32)
    private Date createTime;
    @Column(name = "update_time", nullable = false, length = 32)
    private Date updateTime;

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
