package com.yhh.data.controller;

import com.yhh.data.utils.JSONUtil;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AbstractController {
    public Logger logger = LoggerFactory.getLogger(this.getClass());

    public void doPostData(HttpServletResponse response, Object o) throws IOException {
        if (o == null) {
            return;
        }
        JSONObject jsonObject = JSONUtil.objectToJson(o);
        response.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        try {
            out = response.getWriter();
            out.append(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
}
