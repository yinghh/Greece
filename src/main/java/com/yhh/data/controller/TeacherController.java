package com.yhh.data.controller;

import com.yhh.data.entity.Teacher;
import com.yhh.data.service.TeacherService;
import com.yhh.data.utils.JSONUtil;
import com.yhh.data.utils.MapMessage;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/teacher")
public class TeacherController extends AbstractController {

    @Inject
    private TeacherService teacherService;
    @Inject
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/getteacherbyid", method = RequestMethod.GET)
    @ResponseBody
    public void getTeacherById(HttpServletResponse response, HttpServletRequest request) {
        String idStr = request.getParameter("id");
        int id;
        //redis 测试代码
        HashOperations<String, Object, Object> hash = redisTemplate.opsForHash();
        Map<String,Object> map = new HashMap<>();
        map.put("name", "lp");
        map.put("age", "26");
        hash.putAll("lpMap", map);
        logger.info(hash.entries("lpMap").toString());

        MapMessage mapMessage;
        try {
            id = Integer.parseInt(idStr);
            Teacher teacher = teacherService.selectTeacher(id);
            mapMessage = MapMessage.successMessage();
            mapMessage.put("teacher", teacher);
        } catch (NumberFormatException e) {
            logger.error("error id {}", idStr, e);
            mapMessage = MapMessage.errorMessage();
        }
        try {
            doPostData(response, mapMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @RequestMapping(value = "/deleteteacherbyid", method = RequestMethod.GET)
    @ResponseBody
    public void deleteTeacherById(HttpServletResponse response, HttpServletRequest request) {
        String idStr = request.getParameter("id");
        int id = 0;
        MapMessage mapMessage;
        try {
            id = Integer.parseInt(idStr);
            teacherService.deleteTeacher(id);
            mapMessage = MapMessage.successMessage();
        } catch (NumberFormatException e) {
            logger.error("error id {}", id, e);
            mapMessage = MapMessage.errorMessage();
        }
        try {
            doPostData(response, mapMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/saveandupdateteacherbyid", method = RequestMethod.GET)
    @ResponseBody
    public void saveAndUpdateTeacher(HttpServletResponse response, HttpServletRequest request) {
        String data = request.getParameter("data");
        MapMessage mapMessage;
        try {
            Teacher teacher = (Teacher) JSONUtil.JSONToObj(data, Teacher.class);
            if (teacher == null || teacher.getId() == null) {
                mapMessage = MapMessage.errorMessage();
            } else {
                Teacher teacher1 = teacherService.selectTeacher(teacher.getId());
                if (teacher1 == null) {
                    teacherService.saveTeacher(teacher);
                } else {
                    teacherService.updateTeacher(teacher);
                }
                mapMessage = MapMessage.successMessage();
            }
        } catch (NumberFormatException e) {
            logger.error("error data {}", data, e);
            mapMessage = MapMessage.errorMessage();
        }
        try {
            doPostData(response, mapMessage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
