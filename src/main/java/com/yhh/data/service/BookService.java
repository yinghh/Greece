package com.yhh.data.service;

import com.yhh.data.entity.Book;

public interface BookService {
    /**
     * 根据ID查询数据
     */
    Book selectBook(Integer id);

    /**
     * 保存数据
     */
    Integer saveBook(Book book);

    /**
     * 更新数据
     */
    void updateBook(Book book);

    /**
     * 删除数据
     */
    void deleteBook(Book book);
}
