package com.yhh.data.serviceimpl;

import com.yhh.data.dao.BookDao;
import com.yhh.data.entity.Book;
import com.yhh.data.service.BookService;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class BookServiceImpl implements BookService {
    @Inject
    private BookDao bookDao;

    @Override
    public Book selectBook(Integer id) {
        return bookDao.selectBook(id);
    }

    @Override
    public Integer saveBook(Book book) {
        return bookDao.saveBook(book);
    }

    @Override
    public void updateBook(Book book) {
        bookDao.updateBook(book);
    }

    @Override
    public void deleteBook(Book book) {
        bookDao.deleteBook(book);
    }
}
