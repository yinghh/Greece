package com.yhh.data.serviceimpl;

import com.yhh.data.dao.TeacherDao;
import com.yhh.data.entity.Teacher;
import com.yhh.data.service.TeacherService;


import javax.inject.Inject;
import javax.inject.Named;

@Named
public class TeacherServiceImpl implements TeacherService {
    @Inject
    private TeacherDao teacherDao;

    @Override
    public Teacher selectTeacher(Integer id) {
        return teacherDao.selectTeacher(id);
    }

    @Override
    public Integer saveTeacher(Teacher teacher) {
        return teacherDao.saveTeacher(teacher);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherDao.updateTeacher(teacher);
    }

    @Override
    public void deleteTeacher(Integer id) {
        Teacher teacher = this.selectTeacher(id);
        if (teacher != null) {
            teacherDao.deleteTeacher(teacher);
        }
    }
}
