package com.yhh.data.daoimpl;


import com.yhh.data.dao.TeacherDao;
import com.yhh.data.entity.Teacher;

import javax.inject.Named;

@Named
public class TeacherDaoImpl extends AbstractDaoImpl<Teacher> implements TeacherDao {

    @Override
    public Teacher selectTeacher(Integer id) {
        return select(id);
    }


    @Override
    public Integer saveTeacher(Teacher teacher) {
        return (Integer) save(teacher);
    }


    @Override
    public void updateTeacher(Teacher teacher) {
        update(teacher);
    }

    @Override
    public void deleteTeacher(Teacher teacher) {
        delete(teacher);
    }


}
