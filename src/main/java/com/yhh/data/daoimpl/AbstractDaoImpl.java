package com.yhh.data.daoimpl;


import com.yhh.data.utils.ClassUtils;
import lombok.Getter;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
abstract public class AbstractDaoImpl<T extends Serializable> {
    @Resource
    private SessionFactory sessionfactory;

    public Session getSession() {
        return sessionfactory.getCurrentSession();
    }

    @Getter
    private final Class<T> entityClass;

    @SuppressWarnings("unchecked")
    protected AbstractDaoImpl() {
        Class<?> theClass = ClassUtils.filterCglibProxyClass(this.getClass());
        Type genericSuperClass = theClass.getGenericSuperclass();
        ParameterizedType type = (ParameterizedType) genericSuperClass;
        Type[] typeArgs = type.getActualTypeArguments();
        if (typeArgs.length != 1) {
            throw new RuntimeException("miss the theClass");
        } else {
            entityClass = (Class<T>) typeArgs[0];
        }

    }

    @SuppressWarnings("unchecked")
    public List<T> loads(Collection<Integer> ids) {
        Session session = getSession();
        return session.createCriteria(entityClass).add(Restrictions.in("id", ids)).list();
    }


    @SuppressWarnings("unchecked")
    public T select(Serializable id) {
        Session session = getSession();
        return (T) session.get(entityClass, id);
    }

    public Serializable save(T entity) {
        Session session = getSession();
        return session.save(entity);
    }


    public void update(T entity) {
        Session session = getSession();
        session.update(entity);
    }

    public void delete(T entity) {
        Session session = getSession();
        session.delete(entity);
    }
}
