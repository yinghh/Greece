package com.yhh.data.daoimpl;


import com.yhh.data.dao.BookDao;
import com.yhh.data.entity.Book;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.inject.Named;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Named
public class BookDaoImpl extends AbstractDaoImpl<Book> implements BookDao {
    @Override
    public Book selectBook(Integer id) {
        return select(id);
    }

    @Override
    public Map<Integer, Book> selectBookByIds(Collection<Integer> ids) {
        return loads(ids).stream()
                .collect(Collectors.toMap(Book::getId, Function.identity()));
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> selectAllByQuery() {
        Session session = getSession();
        String hql = "from Book";
        Query query = session.createQuery(hql);
        return query.list();
    }

    @Override
    public List<Object[]> selectAllBySqlQuery() {
        Session session = getSession();
        String sql = "select id,name,create_time,update_time from Book";
        Query query = session.createSQLQuery(sql);
        return query.list();
    }

    @Override
    public Integer saveBook(Book book) {
        return (Integer) save(book);
    }

    @Override
    public void updateBook(Book book) {
        update(book);
    }

    @Override
    public void deleteBook(Book book) {
        delete(book);
    }
}
