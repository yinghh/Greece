package com.yhh.data.service;


import com.yhh.data.entity.Book;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:applicationContext.xml"})
public class TestBookService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Inject
    private BookService bookService;

    @Test
    public void selectBook() {
        Book book = bookService.selectBook(1);
        if (book != null) {
            logger.info(book.toString());
        }
    }

    @Test
    public void deleteBook() {
        Book book = bookService.selectBook(1);
        if (book == null) {
            book = new Book();
            book.setName("数学");
            book.setId(1);
            book.setCreateTime(new Date());
            book.setUpdateTime(new Date());
            Integer id = bookService.saveBook(book);
            logger.info(id.toString());
        }
        bookService.deleteBook(book);
    }


    @Test
    public void saveBook() {
        Book book = bookService.selectBook(2);
        if (book != null) {
            bookService.deleteBook(book);
        }
        book = new Book();
        book.setName("数学");
        book.setId(2);
        book.setCreateTime(new Date());
        book.setUpdateTime(new Date());
        Integer id = bookService.saveBook(book);
        logger.info(id.toString());
    }

    @Test
    public void updateBook() {
        Book book = bookService.selectBook(2);
        if (book != null) {
            bookService.deleteBook(book);
        }
        book = new Book();
        book.setName("数学");
        book.setId(2);
        book.setCreateTime(new Date());
        book.setUpdateTime(new Date());
        Integer id = bookService.saveBook(book);
        logger.info(id.toString());
        book.setName("语文");
        bookService.updateBook(book);
    }

}
